package com.example.vstunews

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.await

class ArticleViewModel : ViewModel() {
    private val _articles: MutableLiveData<List<Article>> by lazy {
        MutableLiveData<List<Article>>()
    }
    val articles: LiveData<List<Article>> by lazy {
        _articles
    }

    val clickedArticle: MutableLiveData<Article> by lazy {
        MutableLiveData<Article>()
    }

    fun getList(){
        viewModelScope.launch(Dispatchers.IO){
            val result = Common.api.getArticles("bbc-news", "859b63e4bb9a4a20a38f48d59d513d1c").await()
            _articles.postValue(generateArticles(result))
        }
    }

    private fun generateArticles(response: ArticleResponse?) : List<Article>{
        val listArticles : MutableList<Article> = arrayListOf()
        response?.articles?.forEach {
            listArticles.add(Article(it.title, it.content, it.urlToImg))
        }
        return listArticles
    }
}