package com.example.vstunews

import java.io.Serializable

data class Article(val titleArticle : String, val contentArticle : String, val imageArticle : String) : Serializable{

}
