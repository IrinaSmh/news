package com.example.vstunews

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import retrofit2.http.Url

class ArticleListAdapter(
    private val articles : List<Article>,
    private val onClickListener : OnClickListListener
) : RecyclerView.Adapter<ArticleListAdapter.ViewHolder>() {
    private fun getItem(position: Int) = articles[position]

    override fun onBindViewHolder(holder: ViewHolder, position: Int){
        val article : Article = getItem(position)
        holder.bind(article)
        holder.itemView.setOnClickListener {
            onClickListener.onClicked(article)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun getItemCount() = articles.size


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val title : TextView = itemView.findViewById(R.id.item_title)
        private val content : TextView = itemView.findViewById(R.id.item_content)
        private val img : ImageView = itemView.findViewById(R.id.item_image)

        fun bind(article: Article){
            title.text = article.titleArticle
            content.text = article.contentArticle
            img.load(article.imageArticle)
        }

        companion object {
            fun from(parent: ViewGroup) : ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                return ViewHolder(layoutInflater.inflate(R.layout.row_item, parent, false))
            }
        }
    }
}
interface OnClickListListener {
    fun onClicked(article : Article)
}