package com.example.vstunews

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PartsOfArticleResponse(
    @Json(name = "title")
    val title : String,
    @Json(name = "content")
    val content : String,
    @Json(name = "urlToImage")
    val urlToImg : String
)