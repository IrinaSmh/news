package com.example.vstunews

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.articles_fragment.*


class ArticleListFragment : Fragment() {
    private lateinit var adapter: ArticleListAdapter
    private val model : ArticleViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.articles_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val viewList: RecyclerView = view.findViewById(R.id.list)
            model.getList()
        val listObserver = Observer<List<Article>> { newList ->
            // Update the UI
            adapter = ArticleListAdapter(newList, object : OnClickListListener{
                 override fun onClicked(article: Article) {
                    model.clickedArticle.value = article
                    val artObserver = Observer<Article> {
                        findNavController().navigate(R.id.action_articleListFragment_to_fullArticleFragment)
                    }
                    model.clickedArticle.observe(viewLifecycleOwner, artObserver)
                }
            })
            viewList.adapter = adapter
        }
        model.articles.observe(viewLifecycleOwner, listObserver)


        swipeRefreshLayout.setOnRefreshListener {
            model.getList()
            swipeRefreshLayout.isRefreshing = false
        }

    }
}
