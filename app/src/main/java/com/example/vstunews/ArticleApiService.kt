package com.example.vstunews

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ArticleApiService {
    @GET("top-headlines")
    fun getArticles(@Query("sources") sources: String, @Query("apiKey") key: String) : Call<ArticleResponse>
}