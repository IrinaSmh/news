package com.example.vstunews

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ArticleResponse (
    @Json(name = "articles")
    val articles : List<PartsOfArticleResponse>
)