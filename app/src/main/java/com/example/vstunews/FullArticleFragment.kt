package com.example.vstunews

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.setFragmentResultListener
import coil.api.load

class FullArticleFragment : Fragment() {

    companion object{
        private const val ARGS_NAME = "args_name"

        fun newInstance(article : Article) : FullArticleFragment {
            val fragment = FullArticleFragment()
            val bundle = Bundle()
            bundle.putSerializable("article", article)
            fragment.arguments = bundle
            return fragment
        }
    }

    private val model : ArticleViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.full_article_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

       // val article : Article = this.arguments?.get("article") as Article

        val title : TextView = view.findViewById(R.id.item_title)
        val content : TextView = view.findViewById(R.id.item_content)
        val img : ImageView = view.findViewById(R.id.item_image)

        title.text = model.clickedArticle.value?.titleArticle ?: "Empty article"
        content.text = model.clickedArticle.value?.contentArticle ?: "Empty article"
        img.load(model.clickedArticle.value?.imageArticle)
    }

}