package com.example.vstunews

import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object Common {
    private val BASE_URL = "http://newsapi.org/v2/"
    val api : ArticleApiService = Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(
        MoshiConverterFactory.create())
        .build()
        .create(ArticleApiService::class.java)
}